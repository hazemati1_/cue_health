#ifndef __TWELVE_BIT_FILE_H__
#define __TWELVE_BIT_FILE_H__

#include <stdint.h>

#include "File.h"

typedef uint16_t uint12_t;

typedef struct {
	FILE* file_handler;
	char* file_name; 
	uint16_t low_nibble; 
	uint16_t high_nibble; 
	uint8_t counter;
} TwelveBitFile;

/*
	initialize the twelve bit file 
	@param -- _file_name: the name of the file
	@return a handler to the twelve bit file
*/
FileOperationStatus init_twelve_bit_file(const char* _file_name, TwelveBitFile** _file);

/*
	get the next twelve bits 
	@param: _file -- a handler to the file
	@return the next twelve bits from the file
*/
uint16_t get_next_twelve_bits(TwelveBitFile* _file);

/*
	get the next twelve bits 
	@param: _file -- a handler to the file
	@return the next twelve bits from the file
*/
size_t get_number_of_uint12_in_file(TwelveBitFile* _file);

/*
	get the file contents of a 12 bit file 
	@param: _file -- a handler to the file
	        buffer -- a pointer to a pointer to a buffer 
	@return: the number of bytes in the file
*/
size_t get_12_bit_file_contents(const char* _file, uint12_t** buffer);
/*
	close the 12 bit file 
	@param: _file -- a handler to the file
	@return: The status of the operation
*/
FileOperationStatus close_twelve_bit_file(TwelveBitFile* _file);

#endif // __TWELVE_BIT_FILE_H__
