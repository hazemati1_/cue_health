#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "AsciiFile.h"

#define NUMBER_DIGITS_TWELVE_BIT_NUMBER 4

static void itoah(int number, uint8_t base, char *string) {
	double number_of_digits = (double) log10((double)number) / log10(base);
	int current_digit = 0;
	int x = number_of_digits;

	while(number > 0.0) {
		current_digit = number % base;
		if(current_digit >= 0 && current_digit <= 9) {
			string[x] = current_digit + '0';
		}
		else if(current_digit > 9) {
			string[x] = (current_digit - 10) + 'A';
		}
		number = number / base;
		x--;
	}
}

static void write_number_to_file(AsciiFile* _file, uint16_t number) {
	char ascii_number[NUMBER_DIGITS_TWELVE_BIT_NUMBER]; 
	const int base = 10;
	memset(ascii_number, '\0', NUMBER_DIGITS_TWELVE_BIT_NUMBER);
	itoah(number, base, ascii_number);
	write_string_to_ascii_file(_file, ascii_number); 
	write_string_to_ascii_file(_file, "\n"); 
}

FileOperationStatus init_ascii_file(const char* _file_name, AsciiFile** _file) {
	if(_file_name == NULL) {
		return NULL_PTR_ERROR;
	}
	AsciiFile* file = (AsciiFile*) malloc(sizeof(AsciiFile));
	file->file_handler = open_file(_file_name, ASCII_FILE);
	if(file->file_handler == NULL) {
		return CANNOT_CREATE_FILE;
	}
	*_file = file;
	return SUCCESS;
}

void write_buffer_to_ascii_file(AsciiFile* _file, uint16_t* buffer, size_t size_of_buffer) {
	for(size_t count = 0; count < size_of_buffer; count++) {
		write_number_to_file(_file, buffer[count]);
	}
}

void write_string_to_ascii_file(AsciiFile* _file, const char* string) {
	size_t length = strlen(string);
	for(size_t x = 0; x < length; x++) {
		put_char(_file->file_handler, string[x]);
	}	
}

FileOperationStatus close_ascii_file(AsciiFile* _file) {
	if(close_file(_file->file_handler) == 0) {
		return SUCCESS;
	}
	free(_file);
	return CANNOT_CLOSE_FILE;
}