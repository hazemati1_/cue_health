#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>

#include "File.h"

FILE* open_file(const char* _file_name, FileType _file_type) {
	switch(_file_type) {
		case BINARY_FILE: 
			return fopen(_file_name, "rb+");
		case ASCII_FILE:
			return fopen(_file_name, "w+");
	}
	return NULL;
}

FileOperationStatus get_char(FILE* _file, uint8_t* _buffer) {
	if(_buffer == NULL) {
		return NULL_PTR_ERROR;
	}
	size_t result;
	const uint8_t number_of_elements = 1; 
	result = fread(_buffer, sizeof(uint8_t), number_of_elements, _file);

	if(number_of_elements != result) {
		return CANNOT_READ_FROM_FILE;
	}
	return SUCCESS;
}

FileOperationStatus put_char(FILE* _file, uint8_t _data) {
	const uint8_t number_of_elements = 1; 
	size_t result = fwrite(&_data, 1, number_of_elements, _file);

	if(number_of_elements != result) {
		return CANNOT_WRITE_TO_FILE;
	}
	return SUCCESS;
}

size_t get_number_of_bytes(const char* _file_name) {
	struct stat st; 
	stat(_file_name, &st); 
	size_t size = st.st_size;
	return size;
}

int close_file(FILE* _file) {
	return fclose(_file);
}
