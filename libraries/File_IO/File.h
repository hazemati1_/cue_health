#ifndef __FILE_H__
#define __FILE_H__

#include <stdint.h>
#include <stdio.h>

typedef enum {
	BINARY_FILE = 0, 
	ASCII_FILE = 1
} FileType; 

typedef enum {
	FAILED_TO_OPEN_FILE,
	END_OF_FILE, 
	CANNOT_CLOSE_FILE,
	CANNOT_OPEN_FILE,
	CANNOT_CREATE_FILE,
	CANNOT_WRITE_TO_FILE,
	CANNOT_READ_FROM_FILE,
	NULL_PTR_ERROR,
	SUCCESS
} FileOperationStatus;

/*
	Open a binary file 
	@param: _file_name: the name of the file
			_file_type: the type of file that needs to be opened
	@return: A handler to the file if file was opened successfully
			 NULL: if file could not be opened successfully
*/
FILE* open_file(const char* _file_name, FileType _file_type);

/*
	get the next byte 
	@param: _file: a handler to a file that needs to be read
			_buffer: a buffer to store the byte that is being read
	@return: SUCCESS: if the byte was successfully read
	         NULL_PTR_ERROR: If the buffer is a null pointer
	         CANNOT_READ_FROM_FILE: If file could not be read
*/
FileOperationStatus get_char(FILE* _file, uint8_t* _buffer);

/*
	write the next character to the file 
	@param: _file: a handler to a file that needs to be written
			_data: a buffer to store the byte that is being written
	@return SUCCESS: if the byte was successfully written
	        CANNOT_WRITE_TO_FILE: If _data could not be written to file
*/
FileOperationStatus put_char(FILE* _file, uint8_t _data);

/*
	close the file 
	@param: a pointer to the file object
	@return If the file is closed, then zero is return, on failure EOF is returned 
*/
int close_file(FILE* _file);

/*
	get the number of bytes in a file 
	@param: the name of the file
	@return The number of bytes in a file
*/
size_t get_number_of_bytes(const char* _file_name);

#endif //__FILE_H__