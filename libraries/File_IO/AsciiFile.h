#ifndef __ASCII_FILES_H__
#define __ASCII_FILES_H__

#include <stdint.h>
#include <stdio.h>

#include "File.h"

typedef struct {
	FILE* file_handler;
} AsciiFile;

/*
    initialize the ascii file 
    @param: _file_name: The file name
    		_file: The file handler
    @return: SUCCESS: The file was succesfully
             NULL_PTR_ERROR: _file_name is a null pointer
             CANNOT_CREATE_FILE: Cannot create or open a file with name _file_name
*/
FileOperationStatus init_ascii_file(const char* _file_name, AsciiFile** _file);

/*
    Write to a number to an ASCII file 
    @param: _file_name: The file name
            number: The number that needs to be written to the file
    @return: void
*/
// void write_number_to_file(AsciiFile* _file, uint16_t number);

/*
    Write a buffer to a file 
    @param: _file: A handler to the file that needs to be updated
            buffer: The buffer that should be written to the file
            size: The size of the buffer that we need to write to the file
    @return: void
*/
void write_buffer_to_ascii_file(AsciiFile* _file, uint16_t* buffer, size_t size_of_buffer);

/*
    Write a string to an ASCII file 
    @param: _file: A handler to the file that needs to be updated
            string: The string that needs to be written to the file
    @return: void
*/
void write_string_to_ascii_file(AsciiFile* _file, const char* string);

/*
    Close the ascii file 
    @param: _file: A handler to the file that needs to be updated
    @return: SUCCESS: If file was closed succeessfully
			 CANNOT_CLOSE_FILE: If file could not be closed
*/
FileOperationStatus close_ascii_file(AsciiFile* _file);

#endif //__ASCII_FILES_H__