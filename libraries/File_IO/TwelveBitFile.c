#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "File.h"
#include "TwelveBitFile.h"

#define MASK                   0X0FFF
#define LOW_NIBBLE_MASK        0XF0
#define HIGH_NIBBLE_MASK       0X0F

static void print_debug(uint8_t first_number, uint8_t second_number, uint8_t twelve_bit_number) {
	printf("first number: 0x%x\n", first_number);
	printf("second number: 0x%x\n", second_number);
	printf("twelve_bit_number: 0x%x\n", twelve_bit_number);
}

static uint12_t* get_file_data(TwelveBitFile* _file, size_t number_of_uint12) {
	uint12_t* buffer = (uint12_t*) malloc(number_of_uint12 * sizeof(uint12_t));
	for(size_t count = 0; count <  number_of_uint12; count++) {
		buffer[count] = get_next_twelve_bits(_file);
	}
	return buffer;
}

FileOperationStatus init_twelve_bit_file(const char* _file_name, TwelveBitFile** _file) {
	TwelveBitFile* file = (TwelveBitFile*) malloc(sizeof(TwelveBitFile));
	file->file_name = strdup(_file_name);
	file->file_handler = open_file(file->file_name, BINARY_FILE);
	file->low_nibble = 0;
	file->high_nibble = 0;
	file->counter = 0;
	*_file = file;

	if(file->file_handler == NULL) {
		return FAILED_TO_OPEN_FILE;
	}
	return SUCCESS;
}

size_t get_number_of_uint12_in_file(TwelveBitFile* _file) {
	const size_t number_of_bits_in_byte = 8; 
	const size_t number_of_bits_in_uint12 = 12;
	size_t number_of_bytes = get_number_of_bytes(_file->file_name);
	size_t number_of_uint12_in_file = (number_of_bytes * number_of_bits_in_byte )/ number_of_bits_in_uint12;
	return number_of_uint12_in_file;
}

uint12_t get_next_twelve_bits(TwelveBitFile* _file) {
	TwelveBitFile* file = (TwelveBitFile*) _file;
	uint12_t twelve_bit_number = 0;
	uint8_t first_number;
	uint8_t second_number;

	if((file->counter % 2) == 0) {
		get_char(file->file_handler, &first_number); 
		get_char(file->file_handler, &second_number);
		file->low_nibble = (second_number & LOW_NIBBLE_MASK) >> 4; 
		file->high_nibble = second_number & HIGH_NIBBLE_MASK;
		twelve_bit_number =  (first_number << 4 | (file->low_nibble));
	}
	else {
		get_char(file->file_handler, &first_number); 
		twelve_bit_number =  file->high_nibble << 8 | (first_number);		
	}
	file->counter++;
	return twelve_bit_number;
}

size_t get_12_bit_file_contents(const char* _file, uint12_t** buffer) {
	TwelveBitFile* binary_file;
	size_t buffer_size = 0;
	if(init_twelve_bit_file(_file, &binary_file) == SUCCESS) {
		buffer_size = get_number_of_uint12_in_file(binary_file);
		*buffer = get_file_data(binary_file, buffer_size);
		close_twelve_bit_file(binary_file);
	}
	else {
		printf("Could not open binary file\n");
	}
	return buffer_size;
}

FileOperationStatus close_twelve_bit_file(TwelveBitFile* _file) {
	free(_file->file_name);
	if(close_file(_file->file_handler) == 0) {
		free(_file);
		return SUCCESS;
	} 
	return CANNOT_CLOSE_FILE;
}
