#ifndef SORT_H
#define SORT_H
#include <stdlib.h>
#include <stdint.h>

typedef enum {
	NullPtrError,
	Success
} SortingStatus;
/*
    sort the array 
    @param: arr: the array
			size: the size of the array
    @return:
*/
SortingStatus heap_sort(uint16_t* arr, size_t arr_size);

#endif //SORT_H
