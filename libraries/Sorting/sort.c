#include <stdio.h>
#include "sort.h"

static void swap(uint16_t* _a, uint16_t* _b) {
    int temp = *_a; 
    *_a = *_b; 
    *_b = temp;
}

void max_heapify(uint16_t* arr, size_t arr_size, int i) {
    uint16_t largest = i;
    size_t left_child_index;
    size_t right_child_index;
    uint16_t* left_child;
    uint16_t* right_child;
    uint16_t* root;
    do {
        i = largest;
        left_child_index = 2 * i + 1;
        right_child_index = 2 * i + 2;
        left_child = &arr[left_child_index];
        right_child = &arr[right_child_index];
        root = &arr[largest];

        // if left child is larger than root
        if (left_child_index < arr_size && *left_child > *root) {
            largest = left_child_index;
            root = &arr[largest];
        }
     
        // if right child is larger than largest so far
        if (right_child_index < arr_size && *right_child > *root) {
            largest = right_child_index;
        }
     
        if (largest != i) {
            swap(&arr[i], &arr[largest]);
        }
    }
    while(largest != i);
}

void build_max_heap(uint16_t* arr, size_t n) {
    for (int i = n / 2 - 1; i >= 0; i--) {
        max_heapify(arr, n, i);
    }

}
SortingStatus heap_sort(uint16_t* arr, size_t n) {
    if(arr == NULL) {
        return NullPtrError;
    }
    build_max_heap(arr, n);
    // one by one extract an element from heap
    for (int i = n - 1; i >= 0; i--) {
        // move current root to end
        swap(&arr[0], &arr[i]);
 
        // call max heapify on the reduced heap
        max_heapify(arr, i, 0);
    }
    return Success;
}