#include <stdint.h>
#include <gtest.h>
#include <stdio.h>

#include "TwelveBitFile.h"
#include "File.h"


TEST(TestGetChar, ReadTestBin2_ReturnTheCorrectValues) {
	const char* file_name = "test.bin";
	uint8_t buffer[] ={0xD5, 0x7A, 0xFF, 0x0C, 0X1C, 0xE2, 0x7C, 0X8A, 0XA6, 0X5B, 0X3F, 0X6C};
	size_t size_of_buffer = sizeof(buffer) / sizeof(buffer[0]);
	FILE* file = open_file(file_name, BINARY_FILE);
	uint8_t current_number = 0; 
	EXPECT_EQ(put_char(file, buffer[0]), SUCCESS);
	EXPECT_EQ(put_char(file, buffer[1]), SUCCESS);
	EXPECT_EQ(put_char(file, buffer[2]), SUCCESS);
	// reset the file pointer so that we start reading at the beginning
	close_file(file);
	file = open_file(file_name, BINARY_FILE);
	
	EXPECT_EQ(get_char(file, &current_number), SUCCESS);
	EXPECT_EQ(buffer[0], current_number);

	EXPECT_EQ(get_char(file, &current_number), SUCCESS);
	EXPECT_EQ(buffer[1], current_number);

	get_char(file, &current_number);
	EXPECT_EQ(buffer[2], current_number);
	close_file(file);
}

