#include <stdint.h>
#include <gtest.h>
#include <stdio.h>

#include "TwelveBitFile.h"
#include "File.h"

static uint8_t test_bytes[] = {
	0xD5, 0x7A, 0xFF, 0x0C, 0X1C, 0xE2, 0x7C, 0X8A, 0XA6, 0X5B, 0X3F, 0X6C
};

TEST(TestGetNextTwelveBits, GetNextByteReturnsValidOutput_ReturnNextTwelveBits) {
	const uint12_t first_twelve_bits = 0xD57;
	const uint12_t second_twelve_bits = 0xAFF;
	TwelveBitFile* file; 

	FileOperationStatus status = init_twelve_bit_file("test2.bin", &file);
	EXPECT_EQ(status, SUCCESS);
	EXPECT_EQ(get_next_twelve_bits(file), first_twelve_bits);
	EXPECT_EQ(get_next_twelve_bits(file), second_twelve_bits);
	close_twelve_bit_file(file);
}

TEST(TestInitTwelveBitFile, BinFileReturnsTrue_ReturnSucess) {
	TwelveBitFile* file; 
	FileOperationStatus status = init_twelve_bit_file("test2.bin", &file);
	EXPECT_EQ(status, SUCCESS);
	close_twelve_bit_file(file);
}

TEST(TestInitTwelveBitFile, BinFileIsInvalid_Return_FailedToOpenFile) {
	TwelveBitFile* file; 
	FileOperationStatus status = init_twelve_bit_file("does_not_exist.bin", &file);
	EXPECT_EQ(status, FAILED_TO_OPEN_FILE);
}

TEST(TestGet12BitFileContents, ValidFileName_AddFileContentToBuffer) {
	const uint12_t first_twelve_bits = 0xD57;
	const uint12_t second_twelve_bits = 0xAFF;
	const size_t expected_number_uint12 = 8;
	TwelveBitFile* file; 
	uint12_t* buffer;
	EXPECT_EQ(get_12_bit_file_contents("test2.bin", &buffer), expected_number_uint12);
	EXPECT_EQ(buffer[0], first_twelve_bits);
	EXPECT_EQ(buffer[1], second_twelve_bits);
}

TEST(TestGet12BitFileContents, InvalidFileName_Return_Zero) {
	const size_t expected_number_uint12 = 0;
	uint12_t* buffer;

	EXPECT_EQ(get_12_bit_file_contents("FileDoesNotExist.bin", &buffer), expected_number_uint12);

}
TEST(TestGetNumberOfUint12InFile, Return_NumberOfUint12) {
	uint12_t expected_number_uint12 = 8; 
	TwelveBitFile* file; 
	init_twelve_bit_file("test2.bin", &file);
	EXPECT_EQ(get_number_of_uint12_in_file(file), expected_number_uint12); 
	close_twelve_bit_file(file);
}


