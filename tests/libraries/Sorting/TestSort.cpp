#include <gtest.h>
#include <gmock.h>
#include <iostream>

#include "sort.h"


using namespace std; 
using std::cout;
using ::testing::ElementsAre;

uint16_t unsorted_arr[] = {3415, 2815, 193, 3298, 1992, 2726, 1459, 3948};

TEST(TestHeapSzort, ValidArray_SortTheArray) {
	size_t size_of_arr = sizeof(unsorted_arr) / sizeof(unsorted_arr[0]); 
	heap_sort(unsorted_arr, size_of_arr);
	ASSERT_THAT(unsorted_arr, ElementsAre(193, 1459, 1992, 2726, 2815, 3298, 3415, 3948));
}


TEST(TestHeapSort, UnsortedArrayNull_Return_NullPtrError) {
	size_t size_of_arr = 5; 
	// heap_sort(NULL, size_of_arr);
	ASSERT_THAT(unsorted_arr, ElementsAre(193, 1459, 1992, 2726, 2815, 3298, 3415, 3948));	
}

TEST(TestHeapSort, UnsortedArrayWithOneElement_Return_Array) {
	uint16_t array_with_one_value[] = {9};
	size_t size_of_arr = sizeof(array_with_one_value) / sizeof(array_with_one_value[0]); 
	heap_sort(array_with_one_value, size_of_arr);
	ASSERT_THAT(array_with_one_value, ElementsAre(9));	
}

// check for invalida size 
