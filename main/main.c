#include <stdio.h>
#include <sort.h>

#include "TwelveBitFile.h"
#include "AsciiFile.h"
void print_buffer(uint16_t* buffer, size_t size_of_buffer) {
	printf("buffer output\n");
	for(size_t count = 0; count < size_of_buffer; count++) {
		printf("%d\n", buffer[count]);
	}
}

void write_last_32_values(const char* input_file_name, AsciiFile* ascii_file) {
	uint12_t* buffer = NULL;
	uint12_t* beginning_of_last_32_numbers = NULL;
	const size_t max_number_of_values = 32;
	size_t  buffer_size = get_12_bit_file_contents(input_file_name, &buffer);	
	size_t number_of_bytes_to_write = (buffer_size >= max_number_of_values) ?  max_number_of_values : buffer_size;
	beginning_of_last_32_numbers = buffer + buffer_size - number_of_bytes_to_write;
	write_string_to_ascii_file(ascii_file, "--Last 32 Values--\n");
	write_buffer_to_ascii_file(ascii_file, beginning_of_last_32_numbers, number_of_bytes_to_write);
	free(buffer);
}

void write_sorted_values(const char* input_file_name, AsciiFile* ascii_file) {
	uint12_t* buffer = NULL;
	uint12_t* beginning_of_last_32_numbers = NULL;
	const size_t max_number_of_values = 32;
	size_t buffer_size = get_12_bit_file_contents(input_file_name, &buffer);
	size_t number_of_bytes_to_write = (buffer_size >= max_number_of_values) ?  max_number_of_values : buffer_size;
	beginning_of_last_32_numbers = buffer + buffer_size - number_of_bytes_to_write;
	heap_sort(buffer, buffer_size);
	write_string_to_ascii_file(ascii_file, "--Sorted Max 32 Values--\n");
	write_buffer_to_ascii_file(ascii_file, beginning_of_last_32_numbers, number_of_bytes_to_write);
	free(buffer);
}

int main(int argc, char* argv[])  {
	char* input_file_name; 
	char* output_file_name;
	AsciiFile* ascii_file;
	if(argc == 3) {
		input_file_name = argv[1];
		output_file_name = argv[2];		
		init_ascii_file(output_file_name, &ascii_file);
		write_sorted_values(input_file_name, ascii_file);
		write_last_32_values(input_file_name, ascii_file);
		close_ascii_file(ascii_file);
	}
	return 1;
}